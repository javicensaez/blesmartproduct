﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// La plantilla de elemento Página en blanco está documentada en https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0xc0a

namespace App2
{
    /// <summary>
    /// Página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public static readonly Guid serviceIot = Guid.Parse("caecface-e1d9-11e6-bf01-fe55135034f0");
        public static readonly Guid read1CharacteristicUuid = Guid.Parse("caec2ebc-e1d9-11e6-bf01-fe55135034f1");
        public static readonly Guid read2CharacteristicUuid = Guid.Parse("caec2ebc-e1d9-11e6-bf01-fe55135034f2");
        public static readonly Guid read3CharacteristicUuid = Guid.Parse("caec2ebc-e1d9-11e6-bf01-fe55135034f3");
        public static readonly Guid read4CharacteristicUuid = Guid.Parse("caec2ebc-e1d9-11e6-bf01-fe55135034f4");
        public static readonly Guid write1CharacteristicUuid = Guid.Parse("caec2ebc-e1d9-11e6-bf01-fe55135034f5");
        public static readonly Guid write2CharacteristicUuid = Guid.Parse("caec2ebc-e1d9-11e6-bf01-fe55135034f6");
        public static readonly Guid write3CharacteristicUuid = Guid.Parse("caec2ebc-e1d9-11e6-bf01-fe55135034f7");
        public static readonly Guid write4CharacteristicUuid = Guid.Parse("caec2ebc-e1d9-11e6-bf01-fe55135034f8");

        private String read1variable="";
        private String read2variable = "";
        private String read3variable = "";
        private String read4variable = "";
        private String write1variable = "0";
        private String write2variable = "0";
        private String write3variable = "0";
        private String write4variable = "0";

        public static readonly GattLocalCharacteristicParameters gattReadParameters = new GattLocalCharacteristicParameters
                {
            CharacteristicProperties = GattCharacteristicProperties.Read |
                                       GattCharacteristicProperties.Notify,
            WriteProtectionLevel = GattProtectionLevel.Plain,
            UserDescription = "Read Characteristic"
        };

        public static readonly GattLocalCharacteristicParameters gattWriteParameters = new GattLocalCharacteristicParameters
        {
            CharacteristicProperties = GattCharacteristicProperties.Write |
                               GattCharacteristicProperties.WriteWithoutResponse,
            WriteProtectionLevel = GattProtectionLevel.Plain,
            UserDescription = "Write Characteristic"
        };

        private GattLocalCharacteristic read1Characteristic;
        private GattLocalCharacteristic read2Characteristic;
        private GattLocalCharacteristic read3Characteristic;
        private GattLocalCharacteristic read4Characteristic;

        private GattLocalCharacteristic write1Characteristic;
        private GattLocalCharacteristic write2Characteristic;
        private GattLocalCharacteristic write3Characteristic;
        private GattLocalCharacteristic write4Characteristic;




        private GattServiceProvider serviceProvider;
     

        public MainPage()
        {
            this.InitializeComponent();
            read1variable = read1Slider.Value.ToString();
            read1TextValue.Text = read1variable;
            read2variable = read2Slider.Value.ToString();
            read2TextValue.Text = read1variable;
            read3variable = read3Slider.Value.ToString();
            read3TextValue.Text = read1variable;
            read4variable = read4Slider.Value.ToString();
            read4TextValue.Text = read1variable;
            UpdateUX();
            Task.Run(() => crearServidor());




        }

             async  void crearServidor()
        {
                GattServiceProviderResult result = await GattServiceProvider.CreateAsync(serviceIot);

                if (result.Error == BluetoothError.Success)
                {
                serviceProvider = result.ServiceProvider;
                    // 
                }
            GattLocalCharacteristicResult characteristicResult = await serviceProvider.Service.CreateCharacteristicAsync(read1CharacteristicUuid, gattReadParameters);
            if (characteristicResult.Error != BluetoothError.Success)
            {
                // An error occurred. 
                return;
            }
            read1Characteristic = characteristicResult.Characteristic;
            read1Characteristic.ReadRequested += Read1Characteristic_ReadRequestedAsync;

            characteristicResult = await serviceProvider.Service.CreateCharacteristicAsync(read2CharacteristicUuid, gattReadParameters);
            if (characteristicResult.Error != BluetoothError.Success)
            {
                // An error occurred. 
                return;
            }
            read2Characteristic = characteristicResult.Characteristic;
            read2Characteristic.ReadRequested += Read2Characteristic_ReadRequestedAsync;

            characteristicResult = await serviceProvider.Service.CreateCharacteristicAsync(read3CharacteristicUuid, gattReadParameters);
            if (characteristicResult.Error != BluetoothError.Success)
            {
                // An error occurred. 
                return;
            }
            read3Characteristic = characteristicResult.Characteristic;
            read3Characteristic.ReadRequested += Read3Characteristic_ReadRequestedAsync;

            characteristicResult = await serviceProvider.Service.CreateCharacteristicAsync(read4CharacteristicUuid, gattReadParameters);
            if (characteristicResult.Error != BluetoothError.Success)
            {
                // An error occurred. 
                return;
            }
            read4Characteristic = characteristicResult.Characteristic;
            read4Characteristic.ReadRequested += Read4Characteristic_ReadRequestedAsync;

            characteristicResult = await serviceProvider.Service.CreateCharacteristicAsync(write1CharacteristicUuid, gattWriteParameters);
            if (characteristicResult.Error != BluetoothError.Success)
            {
                // An error occurred. 
                return;
            }
            write1Characteristic = characteristicResult.Characteristic;
            write1Characteristic.WriteRequested += Write1Characteristic_WriteRequestedAsync;

            characteristicResult = await serviceProvider.Service.CreateCharacteristicAsync(write2CharacteristicUuid, gattWriteParameters);
            if (characteristicResult.Error != BluetoothError.Success)
            {
                // An error occurred. 
                return;
            }
            write2Characteristic = characteristicResult.Characteristic;
            write2Characteristic.WriteRequested += Write2Characteristic_WriteRequestedAsync;

            characteristicResult = await serviceProvider.Service.CreateCharacteristicAsync(write3CharacteristicUuid, gattWriteParameters);
            if (characteristicResult.Error != BluetoothError.Success)
            {
                // An error occurred. 
                return;
            }
            write3Characteristic = characteristicResult.Characteristic;
            write3Characteristic.WriteRequested += Write3Characteristic_WriteRequestedAsync;

            characteristicResult = await serviceProvider.Service.CreateCharacteristicAsync(write4CharacteristicUuid, gattWriteParameters);
            if (characteristicResult.Error != BluetoothError.Success)
            {
                // An error occurred. 
                return;
            }
            write4Characteristic = characteristicResult.Characteristic;
            write4Characteristic.WriteRequested += Write4Characteristic_WriteRequestedAsync;


            GattServiceProviderAdvertisingParameters advParameters = new GattServiceProviderAdvertisingParameters
            {
                IsDiscoverable = true,
                IsConnectable = true
            };
            serviceProvider.StartAdvertising(advParameters); 

        }

        async void Write1Characteristic_WriteRequestedAsync(GattLocalCharacteristic sender, GattWriteRequestedEventArgs args)
        {
            using (args.GetDeferral())
            {

                GattWriteRequest request = await args.GetRequestAsync();
              
                var reader = DataReader.FromBuffer(request.Value);
                // Parse data as necessary. 
                write1variable = reader.ReadString(request.Value.Length);
                if (request.Option == GattWriteOption.WriteWithResponse)
                {
                    request.Respond();
                }
                UpdateUX();
            }//deferral.Complete();
        }

        async void Write2Characteristic_WriteRequestedAsync(GattLocalCharacteristic sender, GattWriteRequestedEventArgs args)
        {
            using (args.GetDeferral())
            {

                GattWriteRequest request = await args.GetRequestAsync();

                var reader = DataReader.FromBuffer(request.Value);
                // Parse data as necessary. 
                write2variable = reader.ReadString(request.Value.Length);
                if (request.Option == GattWriteOption.WriteWithResponse)
                {
                    request.Respond();
                }
                UpdateUX();
            }//deferral.Complete();
        }

        async void Write3Characteristic_WriteRequestedAsync(GattLocalCharacteristic sender, GattWriteRequestedEventArgs args)
        {
            using (args.GetDeferral())
            {

                GattWriteRequest request = await args.GetRequestAsync();

                var reader = DataReader.FromBuffer(request.Value);
                // Parse data as necessary. 
                write3variable = reader.ReadString(request.Value.Length);
                if (request.Option == GattWriteOption.WriteWithResponse)
                {
                    request.Respond();
                }
                UpdateUX();
            }//deferral.Complete();
        }

        async void Write4Characteristic_WriteRequestedAsync(GattLocalCharacteristic sender, GattWriteRequestedEventArgs args)
        {
            using (args.GetDeferral())
            {

                GattWriteRequest request = await args.GetRequestAsync();

                var reader = DataReader.FromBuffer(request.Value);
                // Parse data as necessary. 
                write4variable = reader.ReadString(request.Value.Length);
                if (request.Option == GattWriteOption.WriteWithResponse)
                {
                    request.Respond();
                }
                UpdateUX();
            }//deferral.Complete();
        }

        private  async void Read1Characteristic_ReadRequestedAsync(GattLocalCharacteristic sender, GattReadRequestedEventArgs args)
        {
            var deferral = args.GetDeferral();
            GattReadRequest request = await args.GetRequestAsync();
            if (request == null)
            {
                return;
            }
            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;
            writer.WriteString(read1variable);
            request.RespondWithValue(writer.DetachBuffer());
            deferral.Complete();
        }

        private async void Read2Characteristic_ReadRequestedAsync(GattLocalCharacteristic sender, GattReadRequestedEventArgs args)
        {
            var deferral = args.GetDeferral();
            GattReadRequest request = await args.GetRequestAsync();
            if (request == null)
            {
                return;
            }
            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;
            writer.WriteString(read2variable);
            request.RespondWithValue(writer.DetachBuffer());
            deferral.Complete();
        }

        private async void Read3Characteristic_ReadRequestedAsync(GattLocalCharacteristic sender, GattReadRequestedEventArgs args)
        {
            var deferral = args.GetDeferral();
            GattReadRequest request = await args.GetRequestAsync();
            if (request == null)
            {
                return;
            }
            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;
            writer.WriteString(read3variable);
            request.RespondWithValue(writer.DetachBuffer());
            deferral.Complete();
        }

        private async void Read4Characteristic_ReadRequestedAsync(GattLocalCharacteristic sender, GattReadRequestedEventArgs args)
        {
            var deferral = args.GetDeferral();
            GattReadRequest request = await args.GetRequestAsync();
            if (request == null)
            {
                return;
            }
            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;
            writer.WriteString(read4variable);
            request.RespondWithValue(writer.DetachBuffer());
            deferral.Complete();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

    
        private void Read1Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            Slider slider = sender as Slider;
            if (slider != null)
            {
                read1variable = read1Slider.Value.ToString();
                read1TextValue.Text = read1variable;
                NotifyValue1();
            }
        }

        private void Read2Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            Slider slider = sender as Slider;
            if (slider != null)
            {
                read2variable = read2Slider.Value.ToString();
                read2TextValue.Text = read2variable;
                NotifyValue2();
            }
        }

        private void Read3Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            Slider slider = sender as Slider;
            if (slider != null)
            {
                read3variable = read3Slider.Value.ToString();
                read3TextValue.Text = read3variable;
                NotifyValue3();
            }
        }

        private void Read4Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            Slider slider = sender as Slider;
            if (slider != null)
            {
                read4variable = read4Slider.Value.ToString();
                read4TextValue.Text = read4variable;
                NotifyValue4();
            }
        }
        private async void UpdateUX()
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
              
                write1TextValue.Text = write1variable;
                write2TextValue.Text = write2variable;
                write3TextValue.Text = write3variable;
                write4TextValue.Text = write4variable;

            });
        }
        async void NotifyValue1()
        {
            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;
            writer.WriteString(read1variable);
            await read1Characteristic.NotifyValueAsync(writer.DetachBuffer());
        }
        async void NotifyValue2()
        {
            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;
            writer.WriteString(read2variable);
            await read2Characteristic.NotifyValueAsync(writer.DetachBuffer());
        }
        async void NotifyValue3()
        {
            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;
            writer.WriteString(read3variable);
            await read3Characteristic.NotifyValueAsync(writer.DetachBuffer());
        }
        async void NotifyValue4()
        {
            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;
            writer.WriteString(read4variable);
            await read4Characteristic.NotifyValueAsync(writer.DetachBuffer());
        }
    }
}
